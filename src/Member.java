public class Member {
    private int mMemberID;
    private String mName;
    private String mSurname;
    private double mHeight;
    private double mWeight;

    public Member(int id, String name, String surname, double height, double weight) {
        this.mMemberID = id;
        this.mName = name;
        this.mSurname = surname;
        this.mHeight = height;
        this.mWeight = weight;
    }

    public void display() {
        System.out.printf("\nID: %s, ", this.getMemberID());
        System.out.printf("Name: %s, ", this.getName());
        System.out.printf("Surname: %s, ", this.getSurname());
        System.out.printf("Weight: %s, ", this.getWeight());
        System.out.printf("Height: %s", this.getHeight());
    }

    public String getWeightStatus() {
        String status = "";
        double bmi = this.getBMI();
        if (bmi <= 18.5)
            status = "Underweight";
        else if (bmi > 18.5 && bmi < 25)
            status = "Normal";
        else if (bmi >= 25 && bmi < 30)
            status = "Overweight";
        else if (bmi >= 30 && bmi < 35)
            status = "Obesity";
        else if (bmi >= 35)
            status = "Extreme Obesity";
        return status;
    }

    private double getBMI() {
        return this.getWeight() / Math.pow(this.getHeight(), 2);
    }

    public int getMemberID() {
        return this.mMemberID;
    }

    public void setMemberID(int id) {
        this.mMemberID = id;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getSurname() {
        return this.mSurname;
    }

    public void setSurname(String surname) {
        this.mSurname = surname;
    }

    public double getHeight() {
        return this.mHeight;
    }

    public void setHeight(double height) {
        this.mHeight = height;
    }

    public double getWeight() {
        return this.mWeight;
    }

    public void setWeight(double weight) {
        this.mWeight = weight;
    }
}