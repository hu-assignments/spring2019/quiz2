import java.util.Scanner;

/**
 * @author M. Şamil Ateşoğlu
 * Date:   31.03.2019
 */
public class Main {

    private static final int OP_ADD_NEW_MEMBER = 1;
    private static final int OP_DISPLAY_ALL_MEMBERS = 2;
    private static final int OP_SEARCH = 3;
    private static final int OP_EXIT = 4;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        SportsCenter sportsCenter = new SportsCenter("Sports Center");

        boolean exiting = false;

        while (!exiting) {
            displayMenu(sportsCenter);

            int operation = scanner.nextInt();

            switch (operation) {
                case OP_ADD_NEW_MEMBER:
                    print("\nADDING A NEW MEMBER\n");
                    if (sportsCenter.getNumberOfMembers() < 10) {
                        Member newMember = new Member(0, "", "", 0, 0);

                        print("Enter name: ");
                        String name = scanner.next();
                        newMember.setName(name);

                        print("Enter surname: ");
                        String surname = scanner.next();
                        newMember.setSurname(surname);

                        print("Enter weight: ");
                        double weight = scanner.nextDouble();
                        newMember.setWeight(weight);

                        print("Enter height: ");
                        double height = scanner.nextDouble();
                        newMember.setHeight(height);

                        sportsCenter.addMember(newMember);
                    } else {
                        print("\nMaximum number of members has been reached!");
                    }
                    break;
                case OP_DISPLAY_ALL_MEMBERS:
                    print("\nPRINTING ALL MEMBERS");
                    sportsCenter.printAllMembers();
                    break;
                case OP_SEARCH:
                    print("\nSEARCHING\n");
                    print("Enter name: ");
                    String name = scanner.next();

                    print("Enter surname: ");
                    String surname = scanner.next();

                    sportsCenter.search(name, surname);
                    break;
                case OP_EXIT:
                    exiting = true;
                    break;
            }
        }

        scanner.close();
    }

    public static void displayMenu(SportsCenter sportsCenter) {
        print(String.format("\n--- %s ---", sportsCenter.getName()));
        print("\n1-Add a new member");
        print("\n2-Display all members");
        print("\n3-Search");
        print("\n4-Exit");
        print("\nEnter your choice: ");
    }

    public static void print(String string) {
        System.out.print(string);
    }
}