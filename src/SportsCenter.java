public class SportsCenter {
    private String mName;
    private Member[] mMembers;
    private int mNumberOfMembers = 0;

    public SportsCenter(String name) {
        this.mMembers = new Member[10];
        this.mName = name;
    }

    public void addMember(Member member) {
        member.setMemberID(mNumberOfMembers);
        this.mMembers[mNumberOfMembers++] = member;
    }

    public Member getMember(int i) {
        return this.mMembers[i];
    }

    public void search(String name, String surname) {
        for (int i = 0; i < this.getNumberOfMembers(); i++) {
            if (name.trim().equalsIgnoreCase(this.getMember(i).getName().trim())
                    && surname.trim().equalsIgnoreCase(this.getMember(i).getSurname().trim())) {
                Main.print(String.format("Weight Status: %s\n", this.getMember(i).getWeightStatus()));
                return;
            }
        }
        System.out.printf("Member with the full name \"%s %s\" not found.\n", name, surname);
    }

    public void printAllMembers() {
        for (int i = 0; i < this.getNumberOfMembers(); i++) {
            this.mMembers[i].display();
        }
        Main.print("\n");
    }

    public int getNumberOfMembers() {
        return this.mNumberOfMembers;
    }

    public String getName() {
        return this.mName;
    }
}